# Viacrucis

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.2.

## Development server

Run 'git clone https://gitlab.com/Zuser/via-crucis.git' to clone the repository.
Run 'git pull https://gitlab.com/Zuser/via-crucis' to recover the last version.
Run 'npm install --save' to recover all dependencies.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
