import { HttpHeaders } from '@angular/common/http';

export class HttpOptions {
    private static authTokens = 
    [
        '93a84e2336cc4822b944833505020f2e', 'd14f8d5ad78d493eb60fbe1337a9814b',
        '992639c45a714a06938cf5e032fbb5d1', 'ca7031fc44e744648ed2f53c6849771f',
        'a4024048cf84478fa8dda6f30ee5f39c', 'a500cb1d7d1f420b83fedc59230a4c04',
        '4607e528dc4244b981e7e091ceacf818', 'a075feb92106487a95ba282b47ac07c3'
    ];
    private static requestCounter: number = 0;
    private static requestLimit: number = 1;
    private static selectedToken: number = 0;

    constructor(){}

    static getInstance() : any
    {
        const out = {headers: {'X-Auth-Token': HttpOptions.authTokens[HttpOptions.selectedToken]}};
        if(++HttpOptions.requestCounter >= HttpOptions.requestLimit)
            HttpOptions.selectedToken = (HttpOptions.selectedToken + 1) % HttpOptions.authTokens.length;
        console.log("TOKEN --> " + HttpOptions.authTokens[HttpOptions.selectedToken]);
        return out;
    }
}
