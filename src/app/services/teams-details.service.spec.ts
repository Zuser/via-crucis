import { TestBed } from '@angular/core/testing';

import { TeamsDetailsService } from './teams-details.service';

describe('MatchDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamsDetailsService = TestBed.get(TeamsDetailsService);
    expect(service).toBeTruthy();
  });
});
