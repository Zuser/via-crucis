import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CacheService } from './cache.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsDetailsService {


  constructor(private cache: CacheService) 
  {
    this.cache.setBaseTeamsUrl('http://api.football-data.org/v2/competitions/SA/teams');
    this.cache.setBaseTeamsSpecificDetailsUrl('http://api.football-data.org/v2/teams/');
  }

  getAllTeamDetails(): Observable<any>
  {
    return this.cache.getAllTeamsDetails();
  }

  getTeamDetails(teamId: number): Observable<any>
  {
    return this.cache.getTeamDetails(teamId);
  }
}
