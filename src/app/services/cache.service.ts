import { Injectable } from '@angular/core';
import { HttpOptions } from './http-options';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  private matchesObservable: any;
  private matchesData: any;
  private teamsObservable: any;
  private teamsData: any
  private baseMatchesUrl: string;
  private baseTeamsUrl: string;
  private baseTeamsSpecificDetailsUrl: string;
  private teamsSpecificDetails: any;
  private teamsSpecificObservable: any;

  constructor(private http: HttpClient) 
  { 
    this.matchesObservable = {};
    this.matchesData = {};
    this.teamsSpecificDetails = {};
    this.teamsSpecificObservable = {};
  }

  setBaseMatchesUrl(baseMatchesUrl: string): void
  {
    this.baseMatchesUrl = baseMatchesUrl;
  }

  setBaseTeamsSpecificDetailsUrl(baseTeamsSpecificDetailsUrl: string): void
  {
    this.baseTeamsSpecificDetailsUrl = baseTeamsSpecificDetailsUrl;
  }

  setBaseTeamsUrl(baseTeamsUrl: string): void
  {
    this.baseTeamsUrl = baseTeamsUrl;
  }

  getMatches(day: number) : Observable<any>
  {
    if(this.matchesData[day] != undefined)
      return of(this.matchesData[day]);

    else if(this.matchesObservable[day] != undefined)
      return this.matchesObservable[day];

    else
    {
      const fullUrl = this.baseMatchesUrl + day;
      console.log("Cache miss! --> GET " + fullUrl);
      const options = HttpOptions.getInstance();
      this.matchesObservable[day] = this.http.get<any>(fullUrl, options).pipe(share());
      this.matchesObservable[day].subscribe((data: any) => {
        this.matchesData[day] = data;
        this.matchesObservable[day] = undefined;
      });
      return this.matchesObservable[day]; 
    }
  }

  getAllTeamsDetails() : Observable<any>
  {
    if(this.teamsData != undefined)
      return of(this.teamsData);
    else if(this.teamsObservable != undefined)
      return this.teamsObservable;
    else
    {
      console.log("Cache miss! --> " + this.baseTeamsUrl);
      const options = HttpOptions.getInstance();
      this.teamsObservable = this.http.get<any>(this.baseTeamsUrl, options).pipe(share());
      this.teamsObservable.subscribe((data: any) => {
        this.teamsData = data;
        this.teamsObservable = undefined;
      });
      return this.teamsObservable;
    }
  }

  getTeamDetails(teamId: number)
  {
    if(this.teamsSpecificDetails[teamId] != undefined)
      return of(this.teamsSpecificDetails[teamId])
    else if(this.teamsSpecificObservable[teamId] != undefined)
      return this.teamsSpecificObservable[teamId];
    else
    {
      const options = HttpOptions.getInstance();
      const fullUrl = this.baseTeamsSpecificDetailsUrl + teamId;
      this.teamsSpecificObservable[teamId] = this.http.get<any>(fullUrl, options).pipe(share());
      this.teamsSpecificObservable[teamId].subscribe((data: any) => {
        this.teamsSpecificDetails[teamId] = data;
        this.teamsSpecificObservable[teamId] = undefined;
      });
      return this.teamsSpecificObservable[teamId];
    }
  }
}
