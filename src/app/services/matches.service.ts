import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpOptions } from './http-options';
import { CacheService } from './cache.service';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  private baseUrl: string;
  private competitionId: string;
  ready: EventEmitter<boolean>;

  constructor(private cache: CacheService) 
  { 
    this.cache.setBaseMatchesUrl('http://api.football-data.org/v2/competitions/SA/matches?matchday=');
  }

  getMatches(day: number) : Observable<any>
  {
    return this.cache.getMatches(day);
  }
}
