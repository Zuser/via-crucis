import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatchesPageComponent } from './components/matches-page/matches-page.component';
import { DaysTableComponent } from './components/days-table/days-table.component';
import { MatchesTableComponent } from './components/matches-table/matches-table.component';
import { MatchComponent } from './components/match/match.component';

@NgModule({
  declarations: [
    AppComponent,
    MatchesPageComponent,
    DaysTableComponent,
    MatchesTableComponent,
    MatchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
