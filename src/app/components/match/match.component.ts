import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  @Input() myMatch: any;
  @Input() homeTeamDetails: any;
  @Input() awayTeamDetails: any;

  constructor() {}

  ngOnInit() {}
}
