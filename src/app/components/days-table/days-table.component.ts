import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-days-table',
  templateUrl: './days-table.component.html',
  styleUrls: ['./days-table.component.css']
})
export class DaysTableComponent implements OnInit {
  rows: Array<Number>;
  cols: Array<Number>;
  @Output() dayClickedEvent: EventEmitter<number>; 

  constructor() {
    this.rows = Array.apply(null, Array(2)).map((value, index) => (index + 1));
    this.cols = Array.apply(null, Array(19)).map((value, index) => (index + 1));
    this.dayClickedEvent = new EventEmitter<number>();
   }

  ngOnInit() {
  }

  onDayClick(day: number): void
  {
    this.dayClickedEvent.emit(day);
  }

}
