import { Component, OnInit, ViewChild } from '@angular/core';
import { MatchesTableComponent } from '../matches-table/matches-table.component';
import { TeamsDetailsService } from 'src/app/services/teams-details.service';

@Component({
  selector: 'app-matches-page',
  templateUrl: './matches-page.component.html',
  styleUrls: ['./matches-page.component.css']
})
export class MatchesPageComponent implements OnInit {

  @ViewChild(MatchesTableComponent, {static: false}) mt;
  teamsDetailsData: any;
  teamsArray: Array<any>;
  selectedTeamDetails: any;
  selectedTeamActiveCompetitions: any;
  selectedTeamPlayers: any;
  
  constructor(private tds: TeamsDetailsService) {}

  ngOnInit() 
  {
    this.teamsDetailsData = {};
    this.teamsArray = [];
    this.tds.getAllTeamDetails().subscribe((data: any) => {
      data.teams.map(element => {
        this.teamsDetailsData[element.id] = element;
        this.teamsArray.push(element);
      });
    });
  }

  onDayClickedEvent(day: number): void
  {
    this.mt.onCurrentDayUpdate(day);
  }

  onModalOpen(index: number)
  {
    this.tds.getTeamDetails(this.teamsArray[index].id).subscribe((data) => {
      this.selectedTeamDetails = data;
      this.selectedTeamActiveCompetitions = this.selectedTeamDetails.activeCompetitions;
      this.selectedTeamPlayers = this.selectedTeamDetails.squad;
      setTimeout(() => document.getElementById('teamDetailsModal').style.display = 'block', 50);
    });
  }

  onModalClose(): any
  {
    document.getElementById('teamDetailsModal').style.display = 'none';
  }
}
