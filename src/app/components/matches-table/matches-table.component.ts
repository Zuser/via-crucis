import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { MatchesService } from 'src/app/services/matches.service';

@Component({
  selector: 'app-matches-table',
  templateUrl: './matches-table.component.html',
  styleUrls: ['./matches-table.component.css']
})
export class MatchesTableComponent implements OnInit {

  matches: Array<any>;
  selectedMatch: any;
  @Input() teamDetails: any;
  selectedTeamsDetails: any;

  constructor(private ms: MatchesService) {
  }

  ngOnInit() {
    this.onCurrentDayUpdate(1)
  }

  onCurrentDayUpdate(day: number): void
  {
    this.ms.getMatches(day).subscribe((data: any) => {
       this.matches = data.matches;
    });  
  }

  onModalOpen(index: number): void
  {
    this.selectedMatch = this.matches[index];
    this.selectedTeamsDetails = {
      'homeTeam': this.teamDetails[this.selectedMatch.homeTeam.id], 
      'awayTeam': this.teamDetails[this.selectedMatch.awayTeam.id]
    };
    setTimeout(() => document.getElementById('matchDetailsModal').style.display = 'block', 50);
  }

  onModalClose(): void
  {
    document.getElementById('matchDetailsModal').style.display = 'none';
  }
}
